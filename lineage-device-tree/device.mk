#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-impl.recovery \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Product characteristics
PRODUCT_CHARACTERISTICS := overseas

# Rootdir
PRODUCT_PACKAGES += \
    init.vivo.fingerprint_restart_counter.sh \
    init.vivo.crashdata.sh \
    install-recovery.sh \
    zramsize_reconfig.sh \
    init.vivo.fingerprint.sh \

PRODUCT_PACKAGES += \
    fstab.enableswap \
    factory_init.project.rc \
    init.sensor_1_0.rc \
    init.modem.rc \
    meta_init.modem.rc \
    init.ago.rc \
    init.connectivity.rc \
    init.mt6762.rc \
    factory_init.rc \
    meta_init.connectivity.rc \
    init.project.rc \
    meta_init.project.rc \
    factory_init.connectivity.rc \
    init.factory.rc \
    init.aee.rc \
    init.mt6765.usb.rc \
    meta_init.rc \
    multi_init.rc \
    init.mt6765.rc \

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 28

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/vivo/1901/1901-vendor.mk)
